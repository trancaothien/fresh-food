import 'package:flutter_fresh_food/models/nutrition.dart';

class Food/* extends Nutrition*/ {
  String _name;
  double _price;
  String _description;
  String _storage;
  String _origin;
  String _usage;

  Food(this._name, this._price, this._description, this._storage, this._origin,
      this._usage);

  /*Food(
      int servingSize,
      int calories,
      int protein,
      int carbohydrate,
      int sugar,
      int fibre,
      int fat,
      int saturatedFat,
      int cholesterol,
      int sodium,
      int potassium,
      int vitaminA,
      int vitaminC)
      : super(servingSize, calories, protein, carbohydrate, sugar, fibre, fat,
            saturatedFat, cholesterol, sodium, potassium, vitaminA, vitaminC);*/

  String get usage => _usage;

  set usage(String value) {
    _usage = value;
  }

  String get origin => _origin;

  set origin(String value) {
    _origin = value;
  }

  String get storage => _storage;

  set storage(String value) {
    _storage = value;
  }

  String get description => _description;

  set description(String value) {
    _description = value;
  }

  double get price => _price;

  set price(double value) {
    _price = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }
}
