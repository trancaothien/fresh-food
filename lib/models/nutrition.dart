class Nutrition{
  int _servingSize;
  int _calories;
  int _protein;
  int _carbohydrate;
  int _sugar;
  int _fibre;
  int _fat;
  int _saturatedFat;
  int _cholesterol;
  int _sodium;
  int _potassium;
  int _vitaminA;
  int _vitaminC;

  Nutrition(
      this._servingSize,
      this._calories,
      this._protein,
      this._carbohydrate,
      this._sugar,
      this._fibre,
      this._fat,
      this._saturatedFat,
      this._cholesterol,
      this._sodium,
      this._potassium,
      this._vitaminA,
      this._vitaminC);

  int get vitaminC => _vitaminC;

  set vitaminC(int value) {
    _vitaminC = value;
  }

  int get vitaminA => _vitaminA;

  set vitaminA(int value) {
    _vitaminA = value;
  }

  int get potassium => _potassium;

  set potassium(int value) {
    _potassium = value;
  }

  int get sodium => _sodium;

  set sodium(int value) {
    _sodium = value;
  }

  int get cholesterol => _cholesterol;

  set cholesterol(int value) {
    _cholesterol = value;
  }

  int get saturatedFat => _saturatedFat;

  set saturatedFat(int value) {
    _saturatedFat = value;
  }

  int get fat => _fat;

  set fat(int value) {
    _fat = value;
  }

  int get fibre => _fibre;

  set fibre(int value) {
    _fibre = value;
  }

  int get sugar => _sugar;

  set sugar(int value) {
    _sugar = value;
  }

  int get carbohydrate => _carbohydrate;

  set carbohydrate(int value) {
    _carbohydrate = value;
  }

  int get protein => _protein;

  set protein(int value) {
    _protein = value;
  }

  int get calories => _calories;

  set calories(int value) {
    _calories = value;
  }

  int get servingSize => _servingSize;

  set servingSize(int value) {
    _servingSize = value;
  }
}