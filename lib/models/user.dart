class UserFF{
  String _email;
  String _fullName;

  UserFF(this._email, this._fullName);

  String get phone => _fullName;

  set phone(String value) {
    _fullName = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }
}