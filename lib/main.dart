import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/views/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          return MaterialApp(
            title: 'Fresh Food',
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              fontFamily: 'AcuminPro',
              visualDensity: VisualDensity.adaptivePlatformDensity,
              appBarTheme: AppBarTheme(
                color: Colors.white,
                centerTitle: true,
                elevation: 0,
              ),
              bottomAppBarTheme: BottomAppBarTheme(
                elevation: 0,
              ),
              floatingActionButtonTheme:FloatingActionButtonThemeData(
                elevation: 0,
                backgroundColor: Colors.black,
              ),
              cardTheme: CardTheme(
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
              ),
            ),
            home: Splash(),
          );
        }
      },
    );
  }
}