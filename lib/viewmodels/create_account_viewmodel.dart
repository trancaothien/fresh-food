import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/firebase/firebase_auth.dart';

class CreateAccountViewModel extends ChangeNotifier {
  FireBaseAuth fireBaseAuth;

  void sinUp(String email, String pass, String fullName) {
    fireBaseAuth = FireBaseAuth();
    fireBaseAuth.signUp(pass, email, fullName);
  }
}