import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/firebase/firebase_auth.dart';

class SignInViewModel extends ChangeNotifier {
  // bool validEmail = false;
  // bool validPass = false;
  FireBaseAuth fireBaseAuth;

  Future<String> checkAccount(String email, String pass) async{
    fireBaseAuth = FireBaseAuth();
    String check = await fireBaseAuth.signIn(email, pass);
    return check;
  }
}
