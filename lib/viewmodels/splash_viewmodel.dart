import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';

class SplashViewModel extends ChangeNotifier{
  bool isInitialized = false;
  bool isError = false;

  initFlutterFirebase() async{
    try {
      await Firebase.initializeApp();
      isInitialized = true;
      notifyListeners();
    } catch(e) {
      isError = true;
      notifyListeners();
    }
  }
}