import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
class FireBaseAuth {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  signUp(String pass, String email, String fullName) {
    _firebaseAuth
        .createUserWithEmailAndPassword(email: email, password: pass)
        .then((value) => {_createUser("temp", email, fullName)});
  }

  // signIn(String email, String pass) {
  //   _firebaseAuth
  //       .signInWithEmailAndPassword(email: email, password: pass);
  // }

  Future<String> signIn(String email, String pass) async{
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: pass
      );
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if(e.code == 'user-not-found'){
        return 'user-not-found';
      }else return 'wrong-password';
    }
  }

  _createUser(String userId, String email, String fullName) {
    var user = {"fullName": fullName, "email": email};
    var ref = FirebaseDatabase.instance.reference().child('user');
    ref.child(userId).set(user).then((value) => {}).catchError((onError) {
      print(onError);
    });
  }
}
