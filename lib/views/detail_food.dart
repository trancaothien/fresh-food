import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class DetailFood extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Broccoli',
          style: TextStyle(color: HexColor('#748A9D')),
        ),
        centerTitle: true,
        leading: IconButton(
            icon: Image.asset(
              'assets/images/icon-arrow.png',
              scale: 2,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: BodyDetailFood(),
    );
  }
}

class BodyDetailFood extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
              child: Container(
            child: Image.asset(
              'assets/images/broccoli-1.png',
              fit: BoxFit.cover,
            ),
          )),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 2 - 130,
                    decoration: BoxDecoration(
                        color: HexColor('#F0F4F8'),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10, top: 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      decoration: BoxDecoration(
                        color: HexColor('#7BED8D'),
                        borderRadius: BorderRadius.all(
                          Radius.circular(36.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: Image.asset(
                              'assets/images/icon-arrow-small.png',
                              scale: 1.5,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'SELECT',
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
