import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Store extends StatefulWidget {
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Image.asset(
                'assets/images/icon-search.png',
                scale: 2,
              ),
              onPressed: () {})
        ],
        title: Text(
          'Store',
          style: TextStyle(color: HexColor('#748A9D')),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Image.asset('assets/images/broccoli-1.png',
                            fit: BoxFit.fill),
                        flex: 4,
                      ),
                      Text(
                        'Vegetables',
                        style: TextStyle(
                          fontSize: 30,
                          color: HexColor('#748A9D'),
                        ),
                      ),
                      Text(
                        'Browse',
                        style: TextStyle(
                          fontSize: 20,
                          color: HexColor('#A6BCD0'),
                        ),
                      ),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 10.0), //(x,y)
                      blurRadius: 10.0,
                    ),
                  ],
                ),
              ),
            ),
            flex: 6,
          ),
          Expanded(
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  width: 175,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.asset('assets/images/lemon.png'),
                        Text('Lemon', style: TextStyle(color: Colors.white),),
                      ],
                    ),
                    color: HexColor("#D2F475"),
                    elevation: 2.0,
                  ),
                ),
                Container(
                  width: 175,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.asset('assets/images/pepper.png'),
                        Text('Pepper', style: TextStyle(color: Colors.white),),
                      ],
                    ),
                    color: HexColor("#DD4040"),
                    elevation: 2.0,
                  ),
                ),
                Container(
                  width: 175,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.asset('assets/images/orange.png'),
                        Text('Orange', style: TextStyle(color: Colors.white),),
                      ],
                    ),
                    color: HexColor("#FFA700"),
                    elevation: 2.0,
                  ),
                ),
                Container(
                  width: 175,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.asset('assets/images/banana.png'),
                        Text('Banana', style: TextStyle(color: Colors.white),)
                      ],
                    ),
                    color: HexColor("#FFD958"),
                    elevation: 2.5,
                  ),
                ),
              ],
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }
}
