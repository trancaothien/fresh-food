import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/viewmodels/create_account_viewmodel.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';

TextEditingController _fullName = TextEditingController();
TextEditingController _email = TextEditingController();
TextEditingController _password = TextEditingController();
TextEditingController _confirmPassword = TextEditingController();

class CreateAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CreateAccountViewModel(),
      child: Scaffold(
        appBar: AppBar(),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Text(
                      'Create Account',
                      style:
                      TextStyle(fontSize: 20, color: HexColor('#7BED8D')),
                    ),
                  ),
                ),
                flex: 3,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 20, left: 20),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EditTextFullName(),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 20, left: 20, top: 10),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EditTextEmail(),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 20, left: 20, top: 10),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EditTextPassword(),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 20, left: 20, top: 10),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ConfirmPassword(),
                        ),
                      ),
                    ),
                  ],
                ),
                flex: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50, right: 20, left: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonCreateAccount(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, bottom: 10),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'SIGN IN',
                      style: TextStyle(color: HexColor('#A6BCD0')),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class EditTextFullName extends StatefulWidget {
  @override
  _EditTextFullNameState createState() => _EditTextFullNameState();
}

class _EditTextFullNameState extends State<EditTextFullName> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _fullName,
      decoration: InputDecoration(
        hintText: "Full Name",
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        icon: Image.asset(
          'assets/images/icon-account.png',
          scale: 2,
        ),
      ),
    );
  }
}

class EditTextEmail extends StatefulWidget {
  @override
  _EditTextEmailState createState() => _EditTextEmailState();
}

class _EditTextEmailState extends State<EditTextEmail> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _email,
      decoration: InputDecoration(
        hintText: "Email",
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        icon: Image.asset(
          'assets/images/icon-mail.png',
          scale: 2,
        ),
      ),
    );
  }
}

class EditTextPassword extends StatefulWidget {
  @override
  _EditTextPasswordState createState() => _EditTextPasswordState();
}

class _EditTextPasswordState extends State<EditTextPassword> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _password,
      obscureText: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: "Password",
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        icon: Image.asset(
          'assets/images/icon-padlock.png',
          scale: 2,
        ),
      ),
    );
  }
}

class ConfirmPassword extends StatefulWidget {
  @override
  _ConfirmPasswordState createState() => _ConfirmPasswordState();
}

class _ConfirmPasswordState extends State<ConfirmPassword> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _confirmPassword,
      obscureText: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: "Confirm Password",
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        icon: Image.asset(
          'assets/images/icon-padlock.png',
          scale: 2,
        ),
      ),
    );
  }
}

class ButtonCreateAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final createAccountViewModel = Provider.of<CreateAccountViewModel>(context);
    return InkWell(
      onTap: () {
        createAccountViewModel.sinUp(_email.text, _password.text, _fullName.text);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        decoration: BoxDecoration(
          color: HexColor('#7BED8D'),
          borderRadius: BorderRadius.all(
            Radius.circular(36.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 5),
              child: Image.asset(
                'assets/images/icon-arrow-small.png',
                scale: 1.5,
                color: Colors.white,
              ),
            ),
            Text(
              'CREATE ACCOUNT',
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
