import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/models/food.dart';
import 'package:hexcolor/hexcolor.dart';

class YourOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(25),
        )),
        leading: IconButton(
            icon: Image.asset(
              'assets/images/icon-arrow.png',
              scale: 2,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title:
            Text('Your Orders', style: TextStyle(color: HexColor('#748A9D'))),
      ),
      body: BodyYourOrder(),
    );
  }
}

class BodyYourOrder extends StatefulWidget {
  @override
  _BodyYourOrderState createState() => _BodyYourOrderState();
}

class _BodyYourOrderState extends State<BodyYourOrder>
    with TickerProviderStateMixin {
  List<Food> foods = initValue();

  double _height = 56.0;
  bool _resized = false;

  Animation _arrowAnimation;
  AnimationController _arrowAnimationController;

  @override
  void initState() {
    _arrowAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _arrowAnimation =
        Tween(begin: 0.0, end: pi).animate(_arrowAnimationController);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: foods.length,
        itemBuilder: (context, index) {
          return Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 1),
            child: AnimatedSize(
              curve: Curves.easeInToLinear,
              duration: Duration(seconds: 1),
              vsync: this,
              child: Container(
                height: _height,
                decoration: BoxDecoration(
                  color: HexColor("#F0F4F8"),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ListTile(
                      trailing: InkWell(
                        onTap: () {
                          setState(() {
                            if (_resized) {
                              _arrowAnimationController.isCompleted
                                  ? _arrowAnimationController.reverse()
                                  : _arrowAnimationController.forward();
                              _resized = false;
                              _height = 56.0;
                            } else {
                              _arrowAnimationController.isCompleted
                                  ? _arrowAnimationController.reverse()
                                  : _arrowAnimationController.forward();
                              _resized = true;
                              _height = 320.0;
                            }
                          });
                        },
                        child: AnimatedBuilder(
                            animation: _arrowAnimationController,
                            builder: (context, child) {
                              return Transform.rotate(
                                angle: _arrowAnimation.value,
                                child: Image.asset(
                                    'assets/images/icon-chevron.png'),
                              );
                            }),
                      ),
                      title: Text(
                        'Order #5678',
                        style:
                            TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                      ),
                    ),
                    _resized ? Text('ddđ') : Container(height: 0,),
                    _resized ? Text('ddđ') : Container(height: 0,),
                    _resized ? Text('ddđ') : Container(height: 0,),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

List<Food> initValue() {
  List<Food> foods = new List<Food>();
  foods.add(new Food('rau', 90.0, 'mo ta', '26g', 'sd', 'sd'));
  return foods;
}
