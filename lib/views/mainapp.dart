import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/views/cart.dart';
import 'package:flutter_fresh_food/views/quick_purchase.dart';
import 'package:flutter_fresh_food/views/recipe.dart';
import 'package:flutter_fresh_food/views/setting.dart';
import 'package:flutter_fresh_food/views/store.dart';
import 'package:hexcolor/hexcolor.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {

  PageController _myPage = PageController(initialPage: 0);
  int _pageNumber = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 75,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(left: 28.0),
                icon: Image.asset('assets/images/icon-store.png', color: _pageNumber == 0?HexColor('#7BED8D'): HexColor('#748A9D'),),
                onPressed: () {
                  setState(() {
                    _pageNumber = 0;
                    _myPage.jumpToPage(0);
                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(right: 28.0),
                icon: Image.asset('assets/images/icon-recipes.png', color: _pageNumber == 1?HexColor('#7BED8D'): HexColor('#748A9D'),),
                onPressed: () {
                  setState(() {
                    _pageNumber = 1;
                    _myPage.jumpToPage(1);
                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(left: 28.0),
                icon: Image.asset('assets/images/icon-cart.png', color: _pageNumber == 2?HexColor('#7BED8D'): HexColor('#748A9D'),),
                onPressed: () {
                  setState(() {
                    _pageNumber = 2;
                    _myPage.jumpToPage(2);
                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(right: 28.0),
                icon: Image.asset('assets/images/icon-settings.png', color: _pageNumber == 3?HexColor('#7BED8D'): HexColor('#748A9D'),),
                onPressed: () {
                  setState(() {
                    _pageNumber = 3;
                    _myPage.jumpToPage(3);
                  });
                },
              )
            ],
          ),
        ),
      ),
      body: PageView(
        controller: _myPage,
        onPageChanged: (int) {},
        children: <Widget>[
          Center(
            child: Store(),
          ),
          Center(
            child: Container(
              child: Recipe(),
            ),
          ),
          Center(
            child: Container(
              child: Cart(),
            ),
          ),
          Center(
            child: Container(
              child: Setting(),
            ),
          )
        ],
        physics:
            NeverScrollableScrollPhysics(), // Comment this if you need to use Swipe.
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: EdgeInsets.only(top: 80),
        child: FloatingActionButton(
          child: Image.asset('assets/images/icon-quick-shop.png'),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => QuickPurchase()));
          },
        ),
      ),
    );
  }
}
