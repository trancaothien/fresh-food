import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/views/your_order.dart';
import 'package:hexcolor/hexcolor.dart';

class Setting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Setting', style: TextStyle(color: HexColor('#748A9D'))),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 10, right: 10, left: 10, bottom: 1),
            child: Container(
              decoration: BoxDecoration(
                color: HexColor("#F0F4F8"),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListTile(
                leading: Image.asset(
                  'assets/images/icon-account.png',
                  scale: 2,
                  color: HexColor("#7BED8D"),
                ),
                trailing: Transform.rotate(
                  angle: 300.0,
                  child: Image.asset('assets/images/icon-chevron.png'),
                ),
                // Image.asset('assets/images/icon-chevron.png'),
                title: Text(
                  'Your Account',
                  style: TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
            child: InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => YourOrder()));
              },
              child: Container(
                decoration: BoxDecoration(
                  color: HexColor("#F0F4F8"),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/icon-orders.png',
                    scale: 2,
                    color: HexColor("#7BED8D"),
                  ),
                  trailing: Transform.rotate(
                    angle: 300.0,
                    child: Image.asset('assets/images/icon-chevron.png'),
                  ),
                  title: Text(
                    'Your Orders',
                    style: TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
            child: Container(
              decoration: BoxDecoration(
                color: HexColor("#F0F4F8"),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListTile(
                leading: Image.asset(
                  'assets/images/icon-chat.png',
                  scale: 2,
                  color: HexColor("#7BED8D"),
                ),
                trailing: Transform.rotate(
                  angle: 300.0,
                  child: Image.asset('assets/images/icon-chevron.png'),
                ),
                title: Text(
                  'Live Chat',
                  style: TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
            child: Container(
              decoration: BoxDecoration(
                color: HexColor("#F0F4F8"),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListTile(
                leading: Image.asset(
                  'assets/images/icon-light.png',
                  color: HexColor("#7BED8D"),
                ),
                trailing: Switch(
                  value: false,
                  onChanged: null,
                ),
                title: Text(
                  'Dark Mode',
                  style: TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 327),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: HexColor("#F0F4F8"),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/icon-sign-out.png',
                      scale: 2,
                      color: HexColor("#7BED8D"),
                    ),
                    title: Text(
                      'Sign Out',
                      style:
                          TextStyle(color: HexColor("#748A9D"), fontSize: 20),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
