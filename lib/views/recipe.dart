import 'dart:async';
import 'package:flutter/material.dart';

class Recipe extends StatefulWidget {
  @override
  _RecipeState createState() => _RecipeState();
}

class _RecipeState extends State<Recipe> with TickerProviderStateMixin {
  double _height = 400.0;

  @override
  void initState() {
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _height = 10.0;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        actions: [
          Image.asset(
            'assets/images/icon-search.png',
            scale: 2,
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AnimatedSize(
            curve: Curves.easeInSine,
            duration: Duration(seconds: 1),
            vsync: this,
            child: Container(
              color: Colors.yellow,
              width: MediaQuery.of(context).size.width,
              height: _height,
            ),
          ),
          Text('ạhdasd'),
        ],
      ),
    );
  }
}
