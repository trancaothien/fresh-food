import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class QuickPurchase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Quick Shop',
          style: TextStyle(color: HexColor("#748A9D")),
        ),
        leading: Image.asset(
          'assets/images/icon-refresh.png',
          scale: 1.5,
        ),
        actions: [
          Image.asset(
            'assets/images/icon-select.png',
            color: HexColor('#7BED8D'),
            scale: 1.5,
          ),
        ],
      ),
      body: PageViewQuickPurchase(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: Image.asset(
          'assets/images/icon-quick-shop-two.png',
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

class PageViewQuickPurchase extends StatefulWidget {
  @override
  _PageViewQuickPurchaseState createState() => _PageViewQuickPurchaseState();
}

class _PageViewQuickPurchaseState extends State<PageViewQuickPurchase> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Container(
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Center(
                    child: Text(
                      'All',
                      style:
                          TextStyle(color: HexColor('#A6BCD0'), fontSize: 16),
                    ),
                  )),
                  Expanded(
                      child: Center(
                    child: Text(
                      'Fruit',
                      style:
                          TextStyle(color: HexColor('#A6BCD0'), fontSize: 16),
                    ),
                  )),
                  Expanded(
                      child: Center(
                    child: Text(
                      'Veg',
                      style:
                          TextStyle(color: HexColor('#A6BCD0'), fontSize: 16),
                    ),
                  )),
                  Expanded(
                      child: Center(
                    child: Text(
                      'Nuts',
                      style:
                          TextStyle(color: HexColor('#A6BCD0'), fontSize: 16),
                    ),
                  )),
                ],
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(36),
                  bottomRight: Radius.circular(36)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, 10.0), //(x,y)
                  blurRadius: 10.0,
                ),
              ],
            ),
          ),
          flex: 1,
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.only(bottom: 60.0, top: 10.0, left: 10, right: 10),
            child: GridView.count(
              crossAxisCount: 3,
              crossAxisSpacing: 10,
              mainAxisSpacing: 20,
              children: [
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
                ItemQuickShop(),
              ],
            ),
          ),
          flex: 9,
        )
      ],
    );
  }
}

class ItemQuickShop extends StatefulWidget {
  @override
  _ItemQuickShopState createState() => _ItemQuickShopState();
}

class _ItemQuickShopState extends State<ItemQuickShop> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset('assets/images/icon-colour-apple.png', scale: 2,),
              Text('Apple', style: TextStyle(color: HexColor("#A6BCD0")),),
              Container(
                height: 30,
                width: MediaQuery.of(context).size.width,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('-'),
                      Text('0'),
                      Text('+'),
                    ],
                  ),
                decoration: BoxDecoration(
                  color: HexColor('#F0F4F8'),
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}



