import 'package:flutter/material.dart';
import 'package:flutter_fresh_food/viewmodels/sign_in_viewmodel.dart';
import 'package:flutter_fresh_food/views/create_account.dart';
import 'package:flutter_fresh_food/views/mainapp.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';

TextEditingController _email = TextEditingController();
TextEditingController _pass = TextEditingController();

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => SignInViewModel(),
      child: Scaffold(
        appBar: AppBar(),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Text(
                      'Sign In',
                      style:
                          TextStyle(fontSize: 20, color: HexColor('#7BED8D')),
                    ),
                  ),
                ),
                flex: 3,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 20, left: 20),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EditTextEmail(),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(right: 20, left: 20, top: 10),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: HexColor('#F0F4F8'),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EditTextPass(),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        'Forgot password?',
                        style: TextStyle(color: HexColor('#A6BCD0')),
                      ),
                    ),
                  ],
                ),
                flex: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50, right: 20, left: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonLogin(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, bottom: 10),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreateAccount()));
                    },
                    child: Text(
                      'CREATE ACCOUNT',
                      style: TextStyle(color: HexColor('#A6BCD0')),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class EditTextEmail extends StatefulWidget {
  @override
  _EditTextEmailState createState() => _EditTextEmailState();
}

class _EditTextEmailState extends State<EditTextEmail> {
  @override
  Widget build(BuildContext context) {
    final signInViewModel = Provider.of<SignInViewModel>(context);
    return TextFormField(
      controller: _email,
      decoration: InputDecoration(
        hintText: "Email",
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        // errorText: signInViewModel.validEmail ? 'email is empty' : null,
        icon: Image.asset(
          'assets/images/icon-mail.png',
          scale: 2,
        ),
      ),
    );
  }
}

class EditTextPass extends StatefulWidget {
  @override
  _EditTextPassState createState() => _EditTextPassState();
}

class _EditTextPassState extends State<EditTextPass> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: true,
      controller: _pass,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: 'Password',
        hintStyle: TextStyle(color: HexColor('#A6BCD0')),
        border: InputBorder.none,
        icon: Image.asset(
          'assets/images/icon-padlock.png',
          scale: 2,
        ),
      ),
    );
  }
}

class ButtonLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final signInViewModel = Provider.of<SignInViewModel>(context);
    return InkWell(
      onTap: () async{
        String check = await signInViewModel.checkAccount(_email.text.trim(), _pass.text.trim());
        if (check == 'Success') {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MainApp()));
        } else if(check == 'user-not-found'){
          _showToast(context, 'EMAIL NOT EXIST');
        }else{
          _showToast(context, 'WRONG PASSWORD');
        }

      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        decoration: BoxDecoration(
          color: HexColor('#7BED8D'),
          borderRadius: BorderRadius.all(
            Radius.circular(36.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 5),
              child: Image.asset(
                'assets/images/icon-arrow-small.png',
                scale: 1.5,
                color: Colors.white,
              ),
            ),
            Text(
              'SIGN IN',
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}

void _showToast(BuildContext context, String content) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: Text(content),
      action: SnackBarAction(
        label: 'CLOSE',
        textColor: Colors.red,
        onPressed:scaffold.hideCurrentSnackBar,
      ),
    ),
  );
}
